const http = require("http").createServer(handler);
const io = require("socket.io")(http);
const fs = require("fs");
const request = require('request');

http.listen(3000, '0.0.0.0');

//var redis = require('redis');
//var redisClient = redis.createClient();
//redisClient.set('name', 'artacle');


var connectionList = [];
var totalOnline = 0;

var roomArray1To1 = [];
var roomArray1ToCom = [];
var roomArray2To2 = [];

//storing field info
var gameFields = [];
//timers
let movementTimers = [];

let currentConnectedUserId = 0;


function handler(req, resp) {
    console.log("does handler works?");
    console.log(req);
}


io.on("connection", function(socket) {

    incrementTotalOnlinePlayerCount();

    setCurrentConnectedUserId(socket['id']);
    notifyUserThatConnectionHasBeenEstablished();

    /* CONNECTION RELATED */
    socket.on("addUserToConnectionList", function(data){
        addLog("[UserDetails]", data, 'log')

        if (isUserAlreadyInConnectionLIst(data["userId"])) {
            updateUsersSocketId(data["userId"], socket.id);
        } else {
            insertNewUserToConnectionList(data["userId"], socket.id);
        }

        if (isUserInGameRoom(data)) {
            addLog("[UserHasActiveGame]", 1, 'log')
            joinUserToActiveGameNode(data["userId"], data["roomIdentifier"], socket);
        }
    });

    socket.on('findOrCreateGameRoom', function(data)  {
        findOrCreateGameRoom(data, socket);
    });

    socket.on("cancelGameSearch", function(data) {
        addLog("cancelGameSearch", data, 'log')
        let roomArray = getRoomArray(data["roomType"]);

        cancelSearchForGame(socket, roomArray, data["roomType"], data["userId"]);
    });

    socket.on('reconnectToTheGame', function(data){
        //TODO: complete reconnect part
    });


    socket.on("playerReady", function(data) {
        let keyName = "'"+data["playerId"]+"_"+data["roomId"]+"'";
        addLog("[PlayerReadyKey]", keyName, "log");

        let fieldList = convertUsersFieldList(data["fieldList"]);
        let roomArray = getRoomArray(data["roomType"]);

        let room = roomArray[data["roomName"]];

        addUsersFieldListToArray(fieldList, keyName);
        room = setUserAsReady(room, data["playerId"]);

        addUserToMovementTimer(room, data["playerId"]);

        if (isBothPlayersReady(room, data["playerId"])) {
            notifyUsersThatBothPlayersReady(data["roomName"], room["roomOwner"]);
            startTimer(data["roomName"], room["roomOwner"]);
        } else {
            notifyUser(data["playerId"], "Waiting for second player!");
        }
    });
    /* CONNECTION RELATED END*/

    /* GAME SYSTEM */

    socket.on("attackVirus", function(data){

        let myUserId, enemyUserId;
        myUserId = data["playerId"];

        resetTimerSettingsForUser(data["roomName"], myUserId);

        enemyUserId  = getOpponentUserIdFromRoom(data["playerId"], data["roomName"]);

        let enemyFieldKey = getMyFieldsKey(enemyUserId, data["roomName"], data["roomId"]);
        let myFieldKey = getMyFieldsKey(myUserId, data["roomName"], data["roomId"]);


        let attackedFieldCoordinate = getAttackedFieldCoordinate(data);
        let isFieldHit = false;
        let isFullyDestroyed = false;
        let isGameCompleted = false;

        if (doesUserFieldExist(enemyFieldKey)) {
            if(doesFieldExistOnProvidedCoordinates(enemyFieldKey, attackedFieldCoordinate)) {
                if (doesFieldHaveVirus(enemyFieldKey, attackedFieldCoordinate))
                {
                    isFieldHit = true;
                    markUsersFieldAsHit(enemyFieldKey, attackedFieldCoordinate);
                    markUsersFieldAsEnemyDetected(myFieldKey, attackedFieldCoordinate);

                    if (isVirusFullyDestroyed(enemyFieldKey, attackedFieldCoordinate)) {
                        isFullyDestroyed = true;
                        markFieldsAsDestroyed(myFieldKey, attackedFieldCoordinate, socket);
                    }
                } else {
                    markUsersFieldAsHit(enemyFieldKey, attackedFieldCoordinate);
                    markUsersFieldAsNoEnemyDetected(myFieldKey, attackedFieldCoordinate);
                }
            }
        }

        if (isFullyDestroyed) {
            deductLiveVirusCount(data["roomName"], enemyUserId);

            if (!doesPlayerHaveAnyVirusLeft(data["roomName"], enemyUserId)) {
                isGameCompleted = true;
            }
        }

        if (isGameCompleted) {
            setGameRoomWinner(data["roomName"], myUserId);
            apiUpdateRoomWinner(data["roomName"], myUserId);

            setGameRoomClosed(data["roomName"]);
            announceGameResultToPlayers(myUserId, enemyUserId);
        } else {

            if (!isFullyDestroyed) {
                if (isFieldHit) {
                    notifyUserUpdateFieldToHit(data["locH"], data["locV"], socket);
                } else {
                    notifyUserUpdateFieldToEmpty(data["locH"], data["locV"], socket);
                }
            } else {
                notifyUserUpdateFieldToDestroyed(data["locH"], data["locV"], socket)
            }

            changeUserModeToAttack(enemyUserId, isFieldHit, isFullyDestroyed);
            changeUserModeToDefence(myUserId, isFieldHit, isFullyDestroyed);

            startTimer(data["roomName"], enemyUserId);
        }
    });


    socket.on("requestGameData", function(data) {
        addLog('requestGameData', data, 'log');

        let userId = data["playerId"];
        let usersFieldKey = getMyFieldsKey(userId, data["roomName"], data["roomId"]);
        let usersGameStatus, convertedFields;

        if (doesUserFieldExist(usersFieldKey)) {
            usersGameStatus = getUsersGameStatus(userId, data["roomName"]);
            convertedFields = convertFieldsToReturnableObject(usersFieldKey);
        }

        socket.emit('retrieveGameData', {"fieldList": convertedFields, "gameStatus": usersGameStatus});
    });

    socket.on("giveUpGame", function(data){
        let myUserId, enemyUserId;

        myUserId = data["playerId"];
        enemyUserId  = getOpponentUserIdFromRoom(data["playerId"], data["roomName"]);

        announceGameResultToPlayers(enemyUserId, myUserId);
    });


    /* GAME SYSTEM END */






    socket.on("disconnect", function(socket) {
        decrementTotalOnlinePlayerCount();
    });


    /* ADMIN TOOLS */
    socket.on("adminGetConnectedList", function(data) {
        socket.emit("adminGetConnectedListResult", connectionList);
    });

    socket.on("adminGetRoomList", function(data) {
        let roomArray = getRoomArray(data["roomType"]);
        socket.emit("adminGetRoomList", roomArray);
    });

    socket.on("adminGetSingleRoom", function(data) {
        socket.emit("adminGetRoomList", roomArray1To1);
    });

    socket.on("adminGetUserFields", function() {

    });

    /* ADMIN TOOLS END */


});

/******************************/
/*NEW CODE*/
/******************************/

/**/
/*LOGICAL EVALUATION*/
/**/

function isUserAlreadyInConnectionLIst(userId)
{
    return typeof (connectionList[userId]) != "undefined";
}

function isUserInGameRoom(data)
{
    return data.hasOwnProperty("connectedToRoom") && data["connectedToRoom"];
}

function isRoomAvailableToConnect(room)
{
    if (room.hasOwnProperty('connectedPlayer') && room['connectedPlayer'] == 0 && room['gameStatus'] != "complete") {
        addLog('isRoomAvailableToConnect', room['name'], 'log');
        return true;
    }

    addLog('isRoomAvailableToConnect', room['name'], 'warning');
    return false;
}

function isRoomOwnerStillConnected(room) {
    return typeof(connectionList[room['roomOwner']]) != "undefined";
}

function isBothPlayersReady(room, userId)
{
    addLog('isBothPlayersReady', room , 'log');
    return room["connectedReady"] == 1 && room["ownerReady"] == 1 && room["gameStatus"] == "started";
}

function isVirusFullyDestroyed(userFieldKey, coordinateKey)
{
    let fieldLocationV = (coordinateKey.split("_"))[1];
    let fieldLocationH = (coordinateKey.split("_"))[0];

    if (!isAllConnectedFieldsDownMarkedAsHit(userFieldKey, fieldLocationV, fieldLocationH)) {
        return false;
    }

    if (!isAllConnectedFieldsUpMarkedAsHit(userFieldKey, fieldLocationV, fieldLocationH)) {
        return false;
    }

    if (!isAllConnectedFieldsLeftMarkedAsHit(userFieldKey, fieldLocationV, fieldLocationH)) {
        return false;
    }

    if (!isAllConnectedFieldsRightMarkedAsHit(userFieldKey, fieldLocationV, fieldLocationH)) {
        return false;
    }

    return true;
}

function isAllConnectedFieldsDownMarkedAsHit(fieldKey, locV, locH) {
    addLog('isAllConnectedFieldsDownMarkedAsHit', fieldKey,'log')
    for(let i = (Number(locV)+1); i <= (Number(locV) + 4); i++) {

        let curVirusKey = locH+"_"+i;

        if (!doesFieldExistOnProvidedCoordinates(fieldKey, curVirusKey))
        {
            return true;
        }

        if (doesFieldHaveVirus(fieldKey, curVirusKey) && !doesFieldHasBeenHit()) {
            return false;
        }

        if (!doesFieldHaveVirus(fieldKey, curVirusKey)) {
            return true;
        }
    }

    return true;
}

function isAllConnectedFieldsUpMarkedAsHit(fieldKey, locV, locH)
{
    addLog('isAllConnectedFieldsUpMarkedAsHit', fieldKey,'log')
    for(let i = (Number(locV)-1); i >= (Number(locV) - 4); i--) {

        let curVirusKey = locH+"_"+i;

        if (!doesFieldExistOnProvidedCoordinates(fieldKey, curVirusKey))
        {
            return true;
        }

        if (doesFieldHaveVirus(fieldKey, curVirusKey) && !doesFieldHasBeenHit()) {
            return false;
        }

        if (!doesFieldHaveVirus(fieldKey, curVirusKey)) {
            return true;
        }
    }

    return true;
}

function isAllConnectedFieldsLeftMarkedAsHit(fieldKey, locV, locH)
{
    addLog('isAllConnectedFieldsLeftMarkedAsHit', fieldKey,'log');
    for(let i = (Number(locH)-1); i >= (Number(locH) - 4); i--) {

        let curVirusKey = i+"_"+locV;

        if (!doesFieldExistOnProvidedCoordinates(fieldKey, curVirusKey))
        {
            return true;
        }

        if (doesFieldHaveVirus(fieldKey, curVirusKey) && !doesFieldHasBeenHit()) {
            return false;
        }

        if (!doesFieldHaveVirus(fieldKey, curVirusKey)) {
            return true;
        }
    }

    console.log("[checkFieldsBeenHitLeft] return true, because end of loop");
    return true;
}

function isAllConnectedFieldsRightMarkedAsHit(fieldKey, locV, locH)
{
    addLog('isAllConnectedFieldsRightMarkedAsHit', fieldKey,'log');
    for(let i = (Number(locH)+1); i <= (Number(locH) + 4); i++) {

        let curVirusKey = i+"_"+locV;

        if (!doesFieldExistOnProvidedCoordinates(fieldKey, curVirusKey))
        {
            return true;
        }

        if (doesFieldHaveVirus(fieldKey, curVirusKey) && !doesFieldHasBeenHit()) {
            return false;
        }

        if (!doesFieldHaveVirus(fieldKey, curVirusKey)) {
            return true;
        }
    }

    return true;
}

function isUserTimeExpired(roomName, userId)
{
    addLog('isUserTimeExpired', userId, 'log');
    return movementTimers[roomName][userId]["time"] <= 0;
}

function doesUserFieldExist(userFieldKey)
{
    addLog('doesUserFieldExist', userFieldKey, 'log');
    return typeof (gameFields[userFieldKey]) != "undefined";
}

function doesFieldExistOnProvidedCoordinates(userFieldKey, coordinateKey)
{
    addLog('doesFieldExistOnProvidedCoordinates', coordinateKey, 'log');
    return typeof (gameFields[userFieldKey][coordinateKey]) != "undefined";
}

function doesFieldHaveVirus(userFieldKey, coordinateKey)
{
    addLog('doesFieldHaveVirus', coordinateKey, 'log');
    return gameFields[userFieldKey][coordinateKey]["hasVirus"] == 1;
}

function doesFieldHasBeenHit(userFieldKey, coordinateKey)
{
    addLog('doesFieldHasBeenHit', coordinateKey, 'log');
    return gameFields[userFieldKey][coordinateKey]["hasBeenHit"] == 1;
}

function doesPlayerHaveAnyVirusLeft(roomName, userId)
{
    addLog('doesPlayerHaveLeftViruses', userId, 'log');

    if (roomArray1To1[roomName]["connectedPlayer"] == userId) {
        if(roomArray1To1[roomName]["connectedShipCount"] <= 0) {
            return false;
        }
    }

    if (roomArray1To1[roomName]["roomOwner"] == userId) {
        roomArray1To1[roomName]["ownerShipCount"] = roomArray1To1[roomName]["ownerShipCount"]  - 1;
        if(roomArray1To1[roomName]["ownerShipCount"] <= 0) {
            return false;
        }
    }

    return true;
}

/**/
/*LOGICAL EVALUATION END*/
/**/

function notifyUserThatConnectionHasBeenEstablished() {
    io.to(currentConnectedUserId).emit("connectionEstablished", {status: 1});
}

function notifyUsersThatBothPlayersReady(roomName, ownerId)
{
    addLog('notifyUsersThatBothPlayersReady', roomName, 'log');
    io.in(roomName).emit("bothPlayersReady", {gameStarts: ownerId });
}

function notifyUser (userId, msg)
{
    addLog('notifyUser', userId + "_" + msg, 'log')
    let userSocketId = connectionList[userId]["socketId"];
    io.to(userSocketId).emit("notification", {message: msg});
}

function notifyUserUpdateFieldToDestroyed(locationH, locationV, socket)
{
    addLog('notifyUserUpdateFieldToDestroyed', locationH + "_" + locationV, 'log')
    socket.emit("markFieldAsDestroyed", {locationH: locationH, locationV: locationV});
}

function notifyUserUpdateFieldToHit(locationH, locationV, socket)
{
    addLog('notifyUserUpdateFieldToHit', locationH + "_" + locationV, 'log')
    socket.emit("markFieldsAsHit", {locationH: data["locH"], locationV: data["locV"], type: 2});
}

function notifyUserUpdateFieldToEmpty(locationH, locationV, socket)
{
    //TODO: it should empit markFieldAsEmpty
    addLog('notifyUserUpdateFieldToEmpty', locationH + "_" + locationV, 'log')
    socket.emit("markFieldsAsHit", {locationH: data["locH"], locationV: data["locV"], type: 1});
}

function updateUsersSocketId(userId, socketId)
{
    connectionList[userId]["socketId"] = socketId;
}

function insertNewUserToConnectionList(userId, socketId)
{
    connectionList[userId] = {id: userId, socketId: socketId};
}

function joinUserToActiveGameNode(userId, roomIdentifier, socket)
{
    socket.join(roomIdentifier);
}

function incrementTotalOnlinePlayerCount()
{
    totalOnline++;
}

function decrementTotalOnlinePlayerCount()
{
    totalOnline--;
}

function getRoomArray(roomType)
{
    addLog('[SearchingForRoomType]', roomType, 'log');

    let roomArray = [];

    if (roomType == "onevsone") {
        roomArray = roomArray1To1;
    } else if (roomType == "onevscom") {
        roomArray = roomArray1ToCom;
    } else {
        roomArray = roomArray2To2;
    }

    return roomArray;
}

function findOrCreateGameRoom(data, socket)
{
    let roomArray = getRoomArray(data["roomType"]);
    let roomHasBeenFound = false;

    for(let room in roomArray) {
        if (isRoomAvailableToConnect(roomArray[room])) {

            if (isRoomOwnerStillConnected(roomArray[room])) {

                notifyUserThatRoomHasBeenFound(roomArray[room], socket);
                notifyRoomOwnerThatSomeoneConnectedToRoom(roomArray[room]);

                roomHasBeenFound = true;
            }

        }
    }

    if (!roomHasBeenFound) {
        apiCreateNewGameRoom(socket, data["roomType"], data["userId"]);
    }
}

function cancelSearchForGame(socket, roomArray, roomType, userId)
{
    for(let room in roomArray) {
        if (roomArray[room].hasOwnProperty('roomOwner') && roomArray[room]['roomOwner'] == userId && roomArray[room]['gameStatus'] != "complete") {
            roomArray[room]['gameStatus'] = "complete";

            apiCloseGameRoom(socket, roomType, roomArray[room]['id']);
            return;
        }
    }
}

function canFieldBeMarkedAsDestroyed(userFieldKey, coordinates)
{
    addLog('canFieldBeMarkedAsDestroyed', userFieldKey, 'log')
    return doesFieldHaveVirus(userFieldKey, coordinates) && doesFieldHasBeenHit(userFieldKey, coordinates);
}

function addCreatedRoomToList(socket, data, ownerId)
{
    addLog('addCreatedRoomToList', typeof(data), 'log');
    addLog('addCreatedRoomToList', data, 'log');

    if (data["gameType"] == 1) {
        let roomName = data.roomName;

        let newRoomArray = {
            [roomName] : {
                "id": data.roomId,
                "name": data.roomName,
                "roomOwner": ownerId,
                "connectedPlayer": 0,
                "ownerReady": 0,
                "connectedReady": 0,
                "won": 0,
                "gameStatus": "started",
                "ownerState": 0, //0 - not ready , 1 -attack mode , 2 - defence mode
                "connectedState": 0,
                "connectedShipCount": 10,
                "ownerShipCount": 10
            }
        };

        roomArray1To1.push(newRoomArray);

        /*
                roomArray1To1[data["roomName"]] = {
                    "id": data["roomId"],
                    "name": data["roomName"],
                    "roomOwner": ownerId,
                    "connectedPlayer": 0,
                    "ownerReady": 0,
                    "connectedReady": 0,
                    "won": 0,
                    "gameStatus": "started",
                    "ownerState": 0, //0 - not ready , 1 -attack mode , 2 - defence mode
                    "connectedState": 0,
                    "connectedShipCount": 10,
                    "ownerShipCount": 10
                };

         */
    }

    socket.emit("roomAddedToList", {message: "Room added to list"});
}

function notifyUserThatRoomHasBeenFound(room, socket)
{
    socket.emit('foundRoomAndConnecting', {id: room['id'], name: room['name']});
}

function notifyRoomOwnerThatSomeoneConnectedToRoom(room)
{
    let ownerSocketId = connectionList[room['roomOwner']]['socketId'];
    io.to(ownerSocketId).emit('connectionFound', {roomId: room['id'], roomName: room['name']});
}

function convertUsersFieldList(fieldList)
{
    let convertedFieldList = [];

    for(let field in fieldList) {
        let newKey = fieldList[field]["keyName"];
        convertedFieldList[newKey] = { "keyName": newKey, "hasVirus": fieldList[field]["hasVirus"], "hasBeenHit": fieldList[field]["hasBeenHit"], "enemyStatus": fieldList[field]["enemyStatus"] };
    }

    return convertedFieldList;
}

function addUsersFieldListToArray(fieldList, keyName)
{
    addLog('addUsersFieldListToArray', keyName, 'log')
    gameFields[keyName] = fieldList;
}

function addUserToMovementTimer(roomName, userId)
{
    addLog('addUserToMovementTimer', userId, 'log');
    if (typeof (movementTimers[roomName][userId]) == "undefined") {
        movementTimers[roomName][userId] = {"time": 21, "status": "inactive"};
    }
}

function startTimer(roomName, userId)
{
    addLog('startTimer', userId, 'log');
    movementTimers[roomName][userId]["status"] = "active";

    moveTimer(roomName, userId);
}

function moveTimer(roomName, userId)
{
    addLog('moveTimer', userId, 'log');
    movementTimers[roomName][userId]["time"]--;

    if (isUserTimeExpired(roomName, userId)) {
        resetTimerSettingsForUser(roomName, userId);
        triggerTimeExpired(roomName, userId);
    } else {
        setTimeout(function() { moveTimer(roomName, userId) }, 1000)
    }

    moveTimer(roomName, userId);
}

function resetTimerSettingsForUser(roomName, userId)
{
    addLog('resetTimerSettingsForUser', userId, 'log');
    movementTimers[roomName][userId]["time"] = 21;
    movementTimers[roomName][userId]["status"] = "inactive";
}

function deductLiveVirusCount(roomName, userId)
{
    addLog('deductLiveVirusCount', userId, 'log');

    if (roomArray1To1[roomName]["connectedPlayer"] == userId) {
        roomArray1To1[roomName]["connectedShipCount"] = roomArray1To1[roomName]["connectedShipCount"]  - 1;
    }

    if (roomArray1To1[roomName]["roomOwner"] == userId) {
        roomArray1To1[roomName]["ownerShipCount"] = roomArray1To1[roomName]["ownerShipCount"]  - 1;
    }

    return true;
}



function changeUserModeToAttack(userId, isFieldHit, isFieldDestroyed)
{
    addLog('changeUserModeToAttack', userId, 'log');
    io.to(connectionList[userId]['socketId']).emit("changeToAttack", {"isVirusHit": isFieldHit, "isVirusDestroyed": isFieldDestroyed});
}

function changeUserModeToDefence(userId, isFieldHit, isFieldDestroyed)
{
    addLog('changeUserModeToDefence', userId, 'log');
    io.to(connectionList[userId]['socketId']).emit("changeToDefence", {"isVirusHit": isFieldHit, "isVirusDestroyed": isFieldDestroyed});
}

function changeUserModeToAttackBecauseTimeExpired(userId)
{
    addLog('changeUserModeToAttackBecauseTimeExpired', userId, 'log');
    io.to(connectionList[userId]['socketId']).emit("changeToAttackTimedOut");
}

function changeUserModeToDefenceBecauseTimeExpired(userId)
{
    addLog('changeUserModeToDefenceBecauseTimeExpired', userId, 'log');
    io.to(connectionList[userId]['socketId']).emit("changeToDefenceTimedOut");
}

function triggerTimeExpired(roomName, userId)
{
    let attackerId = userId;
    let defenderId = getOpponentUserIdFromRoom(userId, roomName);

    changeUserModeToAttackBecauseTimeExpired(defenderId);
    changeUserModeToDefenceBecauseTimeExpired(attackerId);

    startTimer(roomName, defenderId);
}

function announceGameResultToPlayers(winnerUserId, looserUserId)
{
    addLog('announceGameResultToPlayers', winnerId, 'log')

    if (typeof(connectionList[winnerUserId]) != "undefined") {
        io.to(connectionList[winnerUserId]['socketId']).emit("gameEnd", {result: "win"});
    }

    if (typeof(connectionList[looserUserId]) != "undefined") {
        io.to(connectionList[looserUserId]['socketId']).emit("gameEnd", {result: "lost"});
    }
}

function convertFieldsToReturnableObject(userFieldKey)
{
    addLog('convertFieldsToReturnableObject', userFieldKey, 'log')
    let currentFields = gameFields[userFieldKey];
    let returnFields = [];

    for(let obj in currentFields) {
        returnFields.push({"keyName": currentFields[obj]["keyName"], "hasVirus": currentFields[obj]["hasVirus"], "hasBeenHit": currentFields[obj]["hasBeenHit"], "enemyStatus": currentFields[obj]["enemyStatus"]})
    }

    return returnFields;
}

/**/
/*API CALLS*/
/**/

function apiUpdateRoomWinner(roomName, winnerId)
{
    addLog('apiUpdateRoomWinner', winnerId, 'log')
    request('http://virus.tripsavage.com/game/complete/'+roomName+'/'+winnerId+'/iD2MaE7IoMWFwuti9D5lEIXimfl3VSr5', function (error, response, body) {
        console.error('error:', error); // Print the error if one occurred
        console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        console.log('body:', body); // Print the HTML for the Google homepage.
    });
}

function apiCreateNewGameRoom(socket, roomType, ownerId)
{
    addLog('[CreateGameApiWithType]', roomType, 'log');
    request('http://virus.tripsavage.com/game/create/gameroom?type='+roomType+'&userid='+ownerId, function (error, response, body) {
        addLog('createNewGameRoom', response, 'warning'); // Print the error if one occurred
        addLog('createNewGameRoom', body, 'log');

        let parsedBody = JSON.parse(body);

        addLog("[ParsedBodyIs]", parsedBody, 'log');
        addLog("[ParsedBodyTypeIs]", typeof(parsedBody), 'log');

        if (parsedBody["status"] == 1) {
            addCreatedRoomToList(socket, parsedBody, ownerId);
        }
    });
}

function apiCloseGameRoom(socket, roomType, roomId)
{
    addLog('apiCloseGameRoom', roomId, 'log');
    request('http://virus.tripsavage.com/game/close/gameroom?roomId='+roomId, function (error, response, body) {
        addLog('createNewGameRoom', response, 'warning'); // Print the error if one occurred
        addLog('createNewGameRoom', body, 'log');

        let parsedBody = JSON.parse(body);

        if (parsedBody["status"] == 1) {
            socket.emit('searchHasBeenCanceled', {id: roomId,  roomType:roomType });
        }
    });
}

/**/
/*END API CALLS*/
/**/


/**/
/*SETTERS*/
/**/
function setCurrentConnectedUserId(connectionId)
{
    addLog('[ConnectionID]', connectionId, 'log')
    currentConnectedUserId = connectionId;
}

function setGameRoomWinner(roomName, winnerId)
{
    addLog('setGameRoomWinner', winnerId, 'log')
    roomArray1To1[roomName]["won"] = winnerId;
}

function setGameRoomClosed(roomName)
{
    addLog('setGameRoomWinner', roomName, 'log')
    roomArray1To1[roomName]["gameStatus"] = "completed";
}

function setUserAsReady(room, userId)
{
    addLog('setUserAsReady', room, 'log')

    if(room["connectedPlayer"] == userId) {
        console.log("Connected Player set to ready");
        room["connectedReady"] = 1;
    }

    if(room["roomOwner"] == userId) {
        console.log("Owner Player set to ready");
        room["ownerReady"] = 1;
    }

    return room;
}

function markUsersFieldAsHit(userFieldKey, coordinateKey)
{
    addLog('markUsersFieldAsHit', userFieldKey, 'log')
    gameFields[userFieldKey][coordinateKey]["hasBeenHit"] = 1;
}

function markUsersFieldAsEnemyDetected(userFieldKey, coordinateKey)
{
    addLog('markUsersFieldAsEnemyDetected', userFieldKey, 'log')
    gameFields[userFieldKey][coordinateKey]["enemyStatus"] = 2;
}

function markUsersFieldAsNoEnemyDetected(userFieldKey, coordinateKey)
{
    addLog('markUsersFieldAsNoEnemyDetected', userFieldKey, 'log')
    gameFields[userFieldKey][coordinateKey]["enemyStatus"] = 1;
}

function markFieldsAsDestroyed(userFieldKey, coordinateKey, socket)
{
    let fieldLocationV = (coordinateKey.split("_"))[1];
    let fieldLocationH = (coordinateKey.split("_"))[0];

    markFieldsDownAsDestroyed(userFieldKey, fieldLocationV, fieldLocationH, socket);
    markFieldsUpAsDestroyed(userFieldKey, fieldLocationV, fieldLocationH, socket);
    markFieldsLeftAsDestroyed(userFieldKey, fieldLocationV, fieldLocationH, socket);
    markFieldsRightAsDestroyed(userFieldKey, fieldLocationV, fieldLocationH, socket);
}

function markFieldsDownAsDestroyed(userFieldKey, fieldLocationV, fieldLocationH, socket)
{
    addLog('markFieldDownAsDestroyed', userFieldKey,'log')
    for(let i = (Number(fieldLocationV)+1); i <= (Number(fieldLocationV) + 4); i++) {

        let curVirusKey = fieldLocationH + "_" + i;

        if (canFieldBeMarkedAsDestroyed(userFieldKey, curVirusKey)) {
            gameFields[userFieldKey][curVirusKey]["enemyStatus"] = 3;
            notifyUserUpdateFieldToDestroyed(fieldLocationH, i, socket);
        }
    }

    return true;
}

function markFieldsUpAsDestroyed(userFieldKey, fieldLocationV, fieldLocationH, socket)
{
    addLog('markFieldUpAsDestroyed', userFieldKey,'log')
    for(let i = (Number(fieldLocationV)-1); i >= (Number(fieldLocationV) - 4); i--) {

        let curVirusKey = fieldLocationH+"_"+i;

        if (canFieldBeMarkedAsDestroyed(userFieldKey, curVirusKey)) {
            gameFields[userFieldKey][curVirusKey]["enemyStatus"] = 3;
            notifyUserUpdateFieldToDestroyed(fieldLocationH, i, socket);
        }
    }
}

function markFieldsLeftAsDestroyed(userFieldKey, fieldLocationV, fieldLocationH, socket)
{
    addLog('markFieldLeftAsDestroyed', userFieldKey,'log');
    for(let i = (Number(fieldLocationH)-1); i >= (Number(fieldLocationH) - 4); i--) {

        let curVirusKey = i+"_"+fieldLocationV;

        if (canFieldBeMarkedAsDestroyed(userFieldKey, curVirusKey)) {
            gameFields[userFieldKey][curVirusKey]["enemyStatus"] = 3;
            notifyUserUpdateFieldToDestroyed(i, fieldLocationV, socket);
        }
    }
}

function markFieldsRightAsDestroyed(userFieldKey, fieldLocationV, fieldLocationH, socket)
{
    addLog('markFieldRightAsDestroyed', userFieldKey,'log');
    for(let i = (Number(fieldLocationH)+1); i <= (Number(fieldLocationH) + 4); i++) {

        let curVirusKey = i+"_"+fieldLocationV;

        if (canFieldBeMarkedAsDestroyed(userFieldKey, curVirusKey)) {
            gameFields[userFieldKey][curVirusKey]["enemyStatus"] = 3;
            notifyUserUpdateFieldToDestroyed(i, fieldLocationV, socket);
        }
    }
}

/**/
/*GETTERS*/
/**/
function getOpponentUserIdFromRoom(userId, roomName)
{
    if (roomArray1To1[roomName]["roomOwner"] == userId) {
        return roomArray1To1[roomName]["connectedPlayer"];
    } else {
        return roomArray1To1[roomName]["roomOwner"];
    }
}

function getMyFieldsKey(userId, roomName, roomId)
{
    //WTF IS THIS?
    let fieldsKey;

    if(typeof(roomArray1To1[roomName]) != "undefined" && roomArray1To1[roomName]["connectedPlayer"] == userId) {
        fieldsKey = "'" + roomArray1To1[roomName]["connectedPlayer"] + "_" + roomId + "'";
    }

    if(typeof(roomArray1To1[data["roomName"]]) != "undefined" && roomArray1To1[data["roomName"]]["roomOwner"] == userId) {
        fieldsKey =  "'" + roomArray1To1[roomName]["roomOwner"] + "_" + roomId + "'";
    }

    return fieldsKey;
}

function getUsersGameStatus(userId, roomName)
{
    addLog("getUsersGameStatus", userId , "log")
    if(typeof(roomArray1To1[roomName]) != "undefined" && roomArray1To1[roomName]["connectedPlayer"] == userId) {
        return roomArray1To1[roomName]["connectedState"];
    }

    if(typeof(roomArray1To1[roomName]) != "undefined" && roomArray1To1[roomName]["roomOwner"] == userId) {
        return roomArray1To1[roomName]["ownerState"];
    }

    return 0;
}

function getAttackedFieldCoordinate(data)
{
    return data["locH"]+"_"+data["locV"];
}


/******************************/
/*LOGGER*/
/******************************/
function addLog(actOrFunction, message, level)
{
    let wrappedAct = "[" + actOrFunction + "]";

    if (typeof (level) != "undefined" && level == "error") {
        console.error(wrappedAct, message);
    } else if (typeof (level) != "undefined" && level == "warning") {
        console.warn(wrappedAct, message);
    } else {
        console.log(wrappedAct, message);
    }
}